#!/usr/bin/env python3

import argparse
import logging
import os
import pathlib
import re
import subprocess
import time
import urllib.parse
import xml.etree.ElementTree as ET

import git
import gitlab
import gitlab.v4.objects
import osc.conf
import osc.core
import tenacity


# ship this until it's shipped in a release:
# https://github.com/python-gitlab/python-gitlab/commit/05cbdc224007e9dda10fc2f6f7d63c82cf36dec0
class ProjectPipelineBridge(gitlab.v4.objects.RESTObject):
    pass


class ProjectPipelineBridgeManager(
    gitlab.v4.objects.ListMixin, gitlab.v4.objects.RESTManager
):
    _path = "/projects/%(project_id)s/pipelines/%(pipeline_id)s/bridges"
    _obj_cls = ProjectPipelineBridge
    _from_parent_attrs = {"project_id": "project_id", "pipeline_id": "id"}
    _list_filters = ("scope",)


SCRATCH_REPO_DIR = "/tmp/scratchrepo"
TEST_BRANCH = "wip/test/fake"
LOCAL_PIPELINE_YAML = """
hello:
  stage: test
  tags: [ lightweight ]
  image: $DOCKER_IMAGE
  script:
    - echo This is a local pipeline
    - ls
    - env --null | sort -z | tr '\\0' '\\n'
  needs:
    - pipeline: $PARENT_PIPELINE_ID
      job: prepare-build-env
      artifacts: true
  rules:
    - when: on_success
"""


def _debian_mangle_version_to_tag(v):
    # see https://dep-team.pages.debian.net/deps/dep14/
    v = v.replace(":", "%")
    v = v.replace("~", "_")
    v = re.sub("\\.(?=\\.|$|lock$)", ".#", v)
    return v


def _get_branch_prefix(project):
    sep = "/"
    prefix = project.default_branch.split(sep)[0]
    return prefix + sep


def _retrying_on_exception(exception_type):
    retrying = tenacity.Retrying(
        stop=tenacity.stop_after_delay(30),
        retry=tenacity.retry_if_exception_type(exception_type),
        wait=tenacity.wait_fixed(1),
    )
    return retrying


def _find_stable_release_branch(project, prefix):
    branches = (b.name for b in project.branches.list(all=True))
    releases = [b for b in branches if b.startswith(prefix)]
    stable = (b for b in releases if b[-4:-1] != "dev" and b[-3:] != "pre")
    # do not pick the latest stable as it may not be actually released yet
    # for instance, in 2021Q1 we have the apertis/v2021 branch but it
    # should not be considered frozen yet since it has not been released
    second_latest = sorted(stable, reverse=True)[1]
    return second_latest


def _monitor_pipeline_for_completion(pipeline):
    logging.info(f"monitoring pipeline {pipeline.web_url}")
    while pipeline.status in ("running", "pending", "created", "waiting_for_resource"):
        logging.debug(f"pipeline {pipeline.web_url} status is {pipeline.status}")
        pipeline.refresh()
        time.sleep(5)


def _commit(gl, repo, filename, msg):
    repo.index.add(filename)
    actor = git.Actor(gl.user.name, gl.user.email)
    commit = repo.index.commit(msg, author=actor, committer=actor)
    logging.info(f"committed {commit.hexsha} to {TEST_BRANCH}")
    return commit


def _push_strict(repo, *args, **kwargs):
    pushes = repo.remotes.origin.push(*args, **kwargs)
    for p in pushes:
        logging.debug(f"push {p.local_ref or ''}:{p.remote_ref} {p.summary.strip()}")
    assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)
    return pushes


class GitLabToOBSTester:
    def __init__(self, reference, scratch, testing_pipeline_url):
        self.apiurl = None
        self.gl = None
        self.reference = reference
        self.scratch = scratch
        self.scratch_git = None
        self.scratch_gitlab = None
        self.stable_branch = None
        self.testing_pipeline_url = testing_pipeline_url

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_auth_token, oscrc):
        if gitlab_server_url:
            logging.info(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_auth_token)
        else:
            logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()
        logging.info(f"Logged to GitLab as {self.gl.user.name} <{self.gl.user.email}>")
        osc.conf.get_config(override_conffile=oscrc)
        self.apiurl = osc.conf.config["apiurl"]

    def _scratch_set_properties(self):
        logging.info(f"setting up properties on {self.scratch}, temporarily disable CI")
        s = self.gl.projects.get(self.scratch)
        self.scratch_gitlab = s
        try:
            s.branches.delete(TEST_BRANCH)
        except gitlab.GitlabDeleteError:
            pass
        s.only_allow_merge_if_pipeline_succeeds = True
        s.merge_requests_access_level = "enabled"
        s.merge_method = "ff"
        s.builds_access_level = "disabled"
        s.lfs_enabled = True
        s.save()

    def _url_add_gitlab_auth(self, orig_url):
        url = urllib.parse.urlparse(orig_url)
        password = urllib.parse.quote(self.gl.private_token, safe="")
        netloc = f"oauth2:{password}@{url.netloc}"
        url = url._replace(netloc=netloc)
        return url.geturl()

    def _scratch_clone(self):
        url = self._url_add_gitlab_auth(self.scratch_gitlab.http_url_to_repo)
        self.scratch_git = git.Repo.clone_from(url, SCRATCH_REPO_DIR)

    def prepare_scratch(self):
        self._scratch_set_properties()
        self._scratch_clone()

    def _scratch_set_variable(self, name, value):
        if not value:
            logging.info(f"deleting CI/CD variable '{name}'")
            self.scratch_gitlab.variables.delete(name)
            return
        logging.info(f"setting CI/CD variable '{name}' to '{value}'")
        vardata = {"key": name, "value": value}
        try:
            self.scratch_gitlab.variables.update(name, vardata)
        except gitlab.GitlabUpdateError:
            self.scratch_gitlab.variables.create(vardata)

    def _scratch_cleanup_tags(self, tags=None):
        scratch = self.scratch_git
        tags = tags if tags else list(scratch.tags)
        logging.debug(f"cleaning up the remote tags {','.join(t.name for t in tags)}")
        for tag in tags:
            try:
                # process tags one by one, so we can ignore failures due to
                # trying to delete from the scratch repository tags that are
                # only available in the reference one
                _push_strict(scratch, tag, delete=True, force=True, verbose=True)
            except git.exc.GitCommandError as e:
                if "remote ref does not exist" not in e.stderr:
                    raise
        scratch.delete_tag(tags)

    def _scratch_synchronize_branches(self, reference):
        scratch = self.scratch_git
        default = reference.default_branch
        logging.debug("synchronizing branches")
        prefix = _get_branch_prefix(reference)
        self.stable_branch = _find_stable_release_branch(reference, prefix)
        branches = [
            default,
            self.stable_branch,
            "pristine-lfs",
            "pristine-lfs-source",
            "debian/buster",
            "debian/bullseye",
        ]
        logging.info(f"synchronizing the {', '.join(branches)} branches")
        for branchname in branches:
            scratch.create_head(
                branchname,
                commit=scratch.remotes.reference.refs[branchname],
                force=True,
            )
        logging.debug(
            f"pushing {branches} to {self.scratch_gitlab.path_with_namespace}"
        )
        git = self.scratch_git.git
        git.lfs("push", "origin", "pristine-lfs-source", "--all")
        _push_strict(scratch, branches, force=True, verbose=True, follow_tags=True)

    def reset_scratch(self):
        scratch = self.scratch_git
        logging.info(
            f"resetting {self.scratch_gitlab.path_with_namespace} to {self.reference}"
        )
        for b in self.scratch_gitlab.branches.list(all=True):
            if b.name.startswith("proposed-updates/"):
                b.delete()
        self._scratch_cleanup_tags()
        reference = self.gl.projects.get(self.reference)
        default = reference.default_branch
        url = self._url_add_gitlab_auth(reference.http_url_to_repo)
        scratch.create_remote("reference", url)
        scratch.remotes.reference.fetch()
        head = scratch.remotes.reference.refs[default]
        logging.debug(f"switching to the {TEST_BRANCH} branch as {head.commit.hexsha}")
        scratch.create_head(TEST_BRANCH, commit=head.commit).checkout()
        self._scratch_synchronize_branches(reference)
        logging.debug(
            f"setting the default branch on {self.scratch_gitlab.path_with_namespace} to {default} and re-enable CI"
        )
        self.scratch_gitlab.default_branch = default
        self.scratch_gitlab.builds_access_level = "enabled"
        self.scratch_gitlab.save()
        self._scratch_set_variable("OBS_PREFIX", self._get_obs_test_project_prefix())
        mrs = self.scratch_gitlab.mergerequests.list(state="opened")
        for mr in mrs:
            mr.state_event = "close"
            mr.save()

    def _get_obs_reference_project_name(self):
        root = self.scratch_gitlab.default_branch.replace("/", ":")
        component = "development"
        filepath = pathlib.Path(SCRATCH_REPO_DIR, "debian/apertis/component")
        if filepath.exists():
            component = open(filepath).read().strip()
        project = f"{root}:{component}"
        return project

    def _get_obs_test_project_prefix(self):
        username = osc.conf.config["api_host_options"][self.apiurl]["user"]
        test_project = f"home:{username}:branches:test:"
        return test_project

    def _get_obs_test_project_name(self):
        prefix = self._get_obs_test_project_prefix()
        reference_project = self._get_obs_reference_project_name()
        test_project = prefix + reference_project
        return test_project

    def obs_prepare_work_areas(self):
        logging.info("preparing work areas on OBS")
        package = self.reference.split("/")[-1]
        src_project = self._get_obs_reference_project_name()
        target_project = self._get_obs_test_project_name()
        msg = "Branch for testing the GitLab-to-OBS pipeline"
        for suffix in ("", ":snapshots"):
            project = target_project + suffix
            logging.info(f"branching {package} from {src_project} to {project}")
            osc.core.branch_pkg(
                self.apiurl,
                src_project,
                package,
                target_project=project,
                msg=msg,
                force=True,
            )
            meta_lines = osc.core.show_project_meta(self.apiurl, project)
            meta_xml = ET.fromstringlist(meta_lines)
            for repository in meta_xml.iter("repository"):
                repository.set("rebuild", "local")
                repository.set("block", "never")
            meta_url = osc.core.make_meta_url(
                "prj", path_args=project, apiurl=self.apiurl
            )
            meta = osc.core.metafile(
                meta_url, ET.tostring(meta_xml, encoding="unicode")
            )
            meta.sync()

    def point_gitlab_ci_here(self, ci_config_path):
        logging.info(f"pointing to the CI definition at {ci_config_path}")
        self.scratch_gitlab.ci_config_path = ci_config_path
        self.scratch_gitlab.save()

    def _mr_wait(self, mr):
        for attempt in _retrying_on_exception(AssertionError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipelines = [
                    p
                    for p in mr.pipelines()
                    if p["sha"] == mr.sha and p["ref"] != mr.source_branch
                ]
                assert (
                    len(pipelines) == 1
                ), f"unexpected number of MR pipelines for commit {mr.sha} in branch {mr.source_branch}: {list(p['id'] for p in pipelines)}"
        mr_pipeline = self.scratch_gitlab.pipelines.get(pipelines[0]["id"])
        _monitor_pipeline_for_completion(mr_pipeline)
        return mr_pipeline

    def test_nonrelease_mr(self):
        logging.info(f"testing an unreleased version on {TEST_BRANCH}")
        assert self.scratch_git.active_branch.name == TEST_BRANCH
        subprocess.run(
            ["dch", "-i", "Fake changes while testing the GitLab-to-OBS pipeline"],
            check=True,
            cwd=SCRATCH_REPO_DIR,
            env=dict(os.environ, DEBEMAIL="test@example.com"),
        )
        msg = f"Test unreleased changes\n\nPipeline: {self.testing_pipeline_url}"
        commit = _commit(self.gl, self.scratch_git, "debian/changelog", msg)
        _push_strict(self.scratch_git, TEST_BRANCH, force=True, verbose=True)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"submitted non-release pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "build-source",
        }, f"submitted non-release pipeline on unmerged changes expected different jobs: {job_names}"
        target = self.scratch_gitlab.default_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(
                source_branch=TEST_BRANCH,
                target_branch=target,
                title="Test non-release",
            )
        )
        logging.info(f"created non-release MR {mr.web_url}")
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "success"
        ), f"mr pipeline {mr_pipeline.web_url} didn't succeed: {mr_pipeline.status}"
        logging.info(f"landing {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=self.scratch_gitlab.default_branch, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"landed non-release pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "build-source",
            "tag-release",
            "upload",
        }, f"landed non-release pipeline expected different jobs: {job_names}"
        logging.info("Non-release MR landed successfully ✅")

    def test_release_commit(self):
        logging.info("submitting a release commit")
        local_pipeline_yaml = "debian/apertis/local-gitlab-ci.yml"
        with pathlib.Path(SCRATCH_REPO_DIR) / local_pipeline_yaml as f:
            f.write_text(LOCAL_PIPELINE_YAML.strip())
        msg = f"Adding a local pipeline in {local_pipeline_yaml}"
        commit = _commit(self.gl, self.scratch_git, str(local_pipeline_yaml), msg)
        subprocess.run(
            ["dch", "-r", "apertis"],
            check=True,
            cwd=SCRATCH_REPO_DIR,
            env=dict(os.environ, DEBEMAIL="test@example.com"),
        )
        msg = f"Test release commit\n\nPipeline: {self.testing_pipeline_url}"
        commit = _commit(self.gl, self.scratch_git, "debian/changelog", msg)
        _push_strict(self.scratch_git, TEST_BRANCH, force=True, verbose=True)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"submitted release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "build-source",
        }, f"submitted release pipeline on unmerged changes expected different jobs: {job_names}"
        bridges = ProjectPipelineBridgeManager(self.gl, pipeline).list()
        assert len(bridges) == 1, "no local dowstream pipeline found"
        name = "local-pipeline-pre-source-build-compat"
        assert bridges[0].name == name, f"no {name} bridge job found"
        return commit

    def test_release_mr_to_frozen_branch(self, commit):
        logging.info("testing MR to frozen branch")
        target = self.stable_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(
                source_branch=TEST_BRANCH,
                target_branch=target,
                title="Test release on frozen branch",
            )
        )
        logging.info(f"created release MR {mr.web_url} to frozen branch {target}")
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "failed"
        ), f"release MR pipeline {mr_pipeline.web_url} to frozen branch should have failed: '{mr_pipeline.status}'"
        job_names = {j.name for j in mr_pipeline.jobs.list()}
        assert job_names == {
            "freeze-stable-branches",
            "prepare-build-env",
            "build-source",
        }, f"submitted non-release pipeline expected different jobs: {job_names}"
        mr.state_event = "close"
        mr.save()
        logging.info("MR on frozen branch blocked successfully ✅")

    def test_release_mr_to_default_branch(self, commit):
        logging.info("testing MR to default branch")
        target = self.scratch_gitlab.default_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(source_branch=TEST_BRANCH, target_branch=target, title="Test release")
        )
        logging.info(f"created release MR {mr.web_url} to default branch {target}")
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "success"
        ), f"mr pipeline {mr_pipeline.web_url} didn't succeed: {mr_pipeline.status}"
        logging.info(f"landing {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=self.scratch_gitlab.default_branch, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"landed release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "build-source",
            "tag-release",
            "upload",
        }, f"landed release pipeline on merged changes expected different jobs: {job_names}"
        logging.info("Release MR landed successfully ✅")

    def test_release_artifacts(self, commit):
        cmd = subprocess.run(
            ["dpkg-parsechangelog", "-SVersion"],
            capture_output=True,
            check=True,
            cwd=SCRATCH_REPO_DIR,
        )
        version = cmd.stdout.decode().strip()
        mangledversion = _debian_mangle_version_to_tag(version)
        prefix = self.scratch_gitlab.default_branch.split("/")[0]
        tagname = f"{prefix}/{mangledversion}"
        self.scratch_git.remotes.origin.fetch(tags=True)
        tag = self.scratch_git.tags[tagname]
        assert (
            tag.commit == commit
        ), f"tag {tagname} points to {tag.commit}, expected {commit}"
        project = self._get_obs_test_project_name()
        package = self.reference.split("/")[-1]
        files = osc.core.meta_get_filelist(self.apiurl, project, package)
        dscs = [f for f in files if f.endswith(".dsc")]
        assert len(dscs) == 1, "expected a single .dsc, got {dscs}"
        dsc = dscs[0]
        expected = f"{package}_{version}.dsc"
        assert dsc == expected, "expected '{expected}' on OBS, got '{dsc}'"
        logging.info(f"Found {tagname} and {dsc} succesfully ✅")

    def test_release_retrigger_pipeline(self, commit):
        branch = self.scratch_gitlab.default_branch
        logging.info(
            f"triggering again a pipeline on the {branch} release branch should have no ill effects"
        )
        pipeline = self.scratch_gitlab.pipelines.create({"ref": branch})
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"the retriggering pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        logging.info("Retriggering pipelines does not fail ✅")

    def test_upstream_pull_up_to_date(self):
        branch = "debian/buster"
        logging.info(
            f"triggering a pipeline on the already up-to-date {branch} upstream branch should succeed"
        )
        pipeline = self.scratch_gitlab.pipelines.create({"ref": branch})
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"the retriggering pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        logging.info("Upstream pull on up-to-date branch succeeded ✅")

    def test_downstream_pull_mirror(self):
        downstream_osname = "downstream"
        source_known_tag = "apertis/0.5.10.2-5co1"
        source_branch = self.scratch_gitlab.default_branch
        release = source_branch.split("/", 1)[-1]
        downstream_branch = downstream_osname + "/" + release
        logging.info(f"testing mirroring from {source_branch} to {downstream_branch}")
        scratch = self.scratch_git
        logging.info(
            f"checking out the old known tag {source_known_tag} (actually commit 93f94fd29342, as the tagged version does not have the mandatory d/a/component file)"
        )
        source_known_tagged_commit = (
            "93f94fd29342"  # scratch.tags[source_known_tag].commit
        )
        scratch.git.checkout()
        logging.info(f"rolling back {source_branch} to {source_known_tag}")
        scratch.create_head(
            source_branch, commit=source_known_tagged_commit, force=True
        )
        scratch.create_head(
            downstream_branch, commit=source_known_tagged_commit, force=True
        )
        logging.info(
            f"disable CI on {self.scratch_gitlab.path_with_namespace} while we roll back branches and tags"
        )
        self.scratch_gitlab.builds_access_level = "disabled"
        self.scratch_gitlab.save()
        newer_tags = [tag for tag in scratch.tags if tag.name > source_known_tag]
        self._scratch_cleanup_tags(newer_tags)
        _push_strict(
            scratch, [source_branch, downstream_branch], force=True, verbose=True
        )
        logging.info(f"re-enable CI on {self.scratch_gitlab.path_with_namespace}")
        self.scratch_gitlab.builds_access_level = "enabled"
        self.scratch_gitlab.save()
        logging.info("trust current licensing information when mirroring")
        self._scratch_set_variable("FORCE_JOB_SKIP", "scan-licenses")
        logging.info(f"trigger mirroring pipeline on {source_branch}")
        pipeline = self.scratch_gitlab.pipelines.create(
            {
                "ref": source_branch,
                "variables": [
                    {"key": "OSNAME", "value": downstream_osname},
                    {
                        "key": "DOCKER_IMAGE",
                        "value": f"$CI_REGISTRY/infrastructure/apertis-docker-images/{release}-package-source-builder",
                    },
                    {
                        "key": "MIRROR_APT_REPOSITORY_PREFIX",
                        "value": "https://repositories.apertis.org/apertis/dists",
                    },
                    {
                        "key": "MIRROR_GIT_REPOSITORY_PREFIX",
                        "value": "https://gitlab.apertis.org/pkg",
                    },
                    {"key": "DOWNSTREAM_RELEASES", "value": downstream_branch},
                    {"key": "FORCE_JOB_RUN", "value": "pull-mirror merge-updates"},
                    {
                        "key": "FORCE_JOB_SKIP",
                        "value": "build-sources tag-release upload obs",
                    },
                ],
            }
        )
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"mirroring pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        scratch.remotes.origin.fetch(tags=True)
        new_commits = list(
            scratch.iter_commits(f"{source_branch}..origin/{source_branch}")
        )
        assert new_commits, "the pull-mirror pipeline didn't import any new commit"
        new_tags = [tag.name for tag in scratch.tags if tag.name > source_known_tag]
        assert new_commits, "the pull-mirror pipeline didn't import any new commit"
        assert new_tags, "the pull-mirror pipeline didn't import any new tag"
        known_tag = "apertis/0.5.10.2-5co2"
        assert (
            known_tag in new_tags
        ), "the expected {known_tag} was not in the imported tags: {new_tags}"
        mrs = [
            mr
            for mr in self.scratch_gitlab.mergerequests.list(state="opened")
            if mr.target_branch == downstream_branch
        ]
        assert (
            mrs
        ), f"the pull-mirror pipeline didn't create any MR targeting {downstream_branch} from {source_branch}"
        assert (
            len(mrs) == 1
        ), "too many MRs after the pull-mirror pipeline run: {[mr.web_url for mr in mrs]}"
        mr = mrs[0]
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "success"
        ), f"mr pipeline {mr_pipeline.web_url} didn't succeed: {mr_pipeline.status}"
        logging.info(f"merging MR {mr.web_url}")
        mr.merge()
        logging.info("Mirroring for downstreams succeeded ✅")
        self._scratch_set_variable("FORCE_JOB_SKIP", None)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test the GitLab-to-OBS pipeline")
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--scratch-repository",
        type=str,
        required=True,
        help="the repository on which the pipeline is tested",
    )
    parser.add_argument(
        "--reference-repository",
        type=str,
        required=True,
        help="the scratch repository will be reset to the reference-repository contents",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--gitlab-auth-token", type=str, help="the GitLab authentication token"
    )
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--oscrc", type=str, help="the OSC configuration with the OBS credentials"
    )
    parser.add_argument(
        "--testing-pipeline-url",
        type=str,
        help="The URL of the pipeline running the test",
    )
    parser.add_argument(
        "--ci-config-path",
        type=str,
        required=True,
        help="The path to the CI config to test",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    if hasattr(gitlab.v4.objects, "ProjectPipelineBridgeManager"):
        logging.warning(
            "python-gitlab now ships ProjectPipelineBridgeManager, the vendored copy can be dropped"
        )

    t = GitLabToOBSTester(
        reference=args.reference_repository,
        scratch=args.scratch_repository,
        testing_pipeline_url=args.testing_pipeline_url,
    )
    t.connect(
        args.gitlab_instance, args.gitlab_server_url, args.gitlab_auth_token, args.oscrc
    )
    t.prepare_scratch()
    t.reset_scratch()
    t.obs_prepare_work_areas()
    t.point_gitlab_ci_here(args.ci_config_path)
    t.test_nonrelease_mr()
    release_commit = t.test_release_commit()
    t.test_release_mr_to_frozen_branch(release_commit)
    t.test_release_mr_to_default_branch(release_commit)
    t.test_release_artifacts(release_commit)
    t.test_release_retrigger_pipeline(release_commit)
    t.test_upstream_pull_up_to_date()
    t.test_downstream_pull_mirror()
    logging.info("Test completed successfully ✨")
