# vi:et:sts=2

variables:
  OSNAME: apertis
  PKG_VERSION_SUFFIX: +apertis
  # we cannot just stick the regex in a variable due to
  # https://gitlab.com/gitlab-org/gitlab/-/issues/35438
  OSNAME_BRANCH_RULE: &osname_branch_rule $CI_COMMIT_BRANCH =~ /^apertis\//
  # run on debian/bullseye and debian/bullseye-security as well,
  # apertis-pkg-pull-updates will always import updates for both
  MIRROR_BRANCH_RULE: &mirror_branch_rule $CI_COMMIT_BRANCH =~ /^debian\/[a-z-]+$/
  OBS_REPOSITORIES: default

  NAME: ${GITLAB_USER_NAME}
  FULLNAME: ${GITLAB_USER_NAME}
  EMAIL: ${GITLAB_USER_EMAIL}
  GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
  GIT_COMMITTER_NAME: ${GITLAB_USER_NAME}
  CI_AUTH_PROJECT_URL: https://${GITLAB_CI_USER}:${GITLAB_CI_PASSWORD}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git/
  GBP_CONF_FILES: "/dev/null"
  LICENSE_BLACKLIST_target: "GPL-3 GPL-3+ AGPL-3 AGPL-3+ AGPL-1 AGPL-1+ LGPL-3 LGPL-3+ BSD-4-Clause MPL-1.1"
  # Set OBS_PREFIX in the project package variables to force uploads to a
  # branch. For instance, by setting it to home:yourusername:branches uploads
  # for apertis:v2020:target will end up in home:yourusername:branches:apertis:v2020:target
  OBS_PREFIX: ""
  DOWNSTREAM_RELEASES_debian_bullseye: apertis/v2022dev3
  DOWNSTREAM_RELEASES_debian_buster: apertis/v2021
  STABLE_RELEASES: apertis/v2019:apertis/v2020:apertis/v2021
  # Colon-separated list of branches for which upstream pulls should be automerged, e.g. apertis/v2022dev1:apertis/v2022dev2, if the MR consists only of a trivial change
  PKG_UPDATES_AUTOMERGE: $CI_DEFAULT_BRANCH
  DEBIAN_MIRROR: https://deb.debian.org/debian
  # disable the submission to FOSSology for now to speed up the rebase
  # we still have the old-style scanner as the default to actually catch licensing issues
  #FOSSOLOGY_URL: https://fossology.apertis.org
  # enable for downstreams packages, pulling from Apertis (see the pull-mirror job)
  #MIRROR_APT_REPOSITORY_PREFIX: https://repositories.apertis.org/apertis/dists
  #MIRROR_GIT_REPOSITORY_PREFIX: https://gitlab.apertis.org/pkg

  # for testing, set those to force jobs to be run or to be skipped
  # if the job name is contained in any way in the string, the matching action is taken
  #FORCE_JOB_RUN: ""
  #FORCE_JOB_SKIP: ""

  # to disable the auto merge feature set PKG_UPDATES_AUTOMERGE_DISABLED to some value
  #PKG_UPDATES_AUTOMERGE_DISABLED: "1"

image: $DOCKER_IMAGE

default:
  interruptible: true
  before_script:
    - mkdir -p .git/info
    - echo '* -text -eol -crlf -ident -filter -working-tree-encoding -export-subst' > .git/info/attributes
    - echo 'debian/changelog merge=dpkg-mergechangelogs' >> .git/info/attributes

.rules:
  - <<: &force_job_rule_skip
      if: $CI_JOB_NAME =~ $FORCE_JOB_SKIP
      when: never
  - <<: &force_job_rule_run
      if: $CI_JOB_NAME =~ $FORCE_JOB_RUN
      when: on_success
  - <<: &run_on_mr_xor_branch_rule
      if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      # do not run on a branch if there's a MR open, run only in MR context so
      # we can derive the release from the target branch
      when: never

stages:
  - build-env
  - update
  - merge
  - build
  - license scan
  - release
  - upload
  - OBS

prepare-build-env:
  stage: build-env
  tags:
    - lightweight
  image: debian:bullseye-slim
  variables:
    GIT_DEPTH: 1
  script:
    - APERTIS_RELEASE_DEFAULT=$(echo $CI_COMMIT_BRANCH | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?' || true)
    - test -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" && APERTIS_RELEASE_DEFAULT=$(echo $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?' || true)
    - APERTIS_RELEASE_DEFAULT=${APERTIS_RELEASE_DEFAULT:-${CI_DEFAULT_BRANCH##$OSNAME/}}
    - APERTIS_RELEASE=${APERTIS_RELEASE:-$APERTIS_RELEASE_DEFAULT}
    - case $APERTIS_RELEASE in
        v2019*|v2020*)
          DOCKER_IMAGE_DEFAULT=docker-registry.apertis.org/apertis/apertis-${APERTIS_RELEASE}-package-source-builder
        ;;
        *)
          DOCKER_IMAGE_DEFAULT=${CI_REGISTRY}/infrastructure/$OSNAME-docker-images/${APERTIS_RELEASE}-package-source-builder
        ;;
      esac
    - DOCKER_IMAGE=${DOCKER_IMAGE:-$DOCKER_IMAGE_DEFAULT}
    - echo "APERTIS_RELEASE=$APERTIS_RELEASE" | tee -a build.env
    - echo "DOCKER_IMAGE=$DOCKER_IMAGE" | tee -a build.env
    - COMPONENT_DEFAULT=$(cat debian/apertis/component || test "${CI_COMMIT_BRANCH%%/*}" = debian)
    - COMPONENT=${COMPONENT:-$COMPONENT_DEFAULT}
    - echo "COMPONENT=$COMPONENT" | tee -a build.env
    - SANITIZED_COMPONENT=$(echo -n "$COMPONENT" | tr -c '[:alnum:]' '_') # replace non alphanum with '_', eg. non-free → non_free
    - LICENSE_BLACKLIST_VAR=LICENSE_BLACKLIST_$SANITIZED_COMPONENT # look for variables like "$LICENSE_BLACKLIST_target"
    - LICENSE_BLACKLIST=${LICENSE_BLACKLIST:-${!LICENSE_BLACKLIST_VAR:-}}
    - echo "LICENSE_BLACKLIST=$LICENSE_BLACKLIST" | tee -a build.env
  artifacts:
    reports:
      dotenv: build.env
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: $CI_COMMIT_BRANCH =~ /^upstream\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^upstream\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^pristine-/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^pristine-\//
      when: never
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
    - when: never

pull-updates:
  stage: update
  tags:
    - lightweight
  resource_group: $CI_PROJECT_ID
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - if: $DEBIAN_MIRROR == null || $DEBIAN_MIRROR == ""
      when: never
    - if: *mirror_branch_rule
      when: on_success
  script:
    - PACKAGE=$(dpkg-parsechangelog -SSource || echo "$CI_PROJECT_NAME")
    - UPSTREAM_RELEASE=$CI_COMMIT_BRANCH
    - >
      apertis-pkg-pull-updates
      --package=${PACKAGE}
      --upstream=${UPSTREAM_RELEASE##*/}
      --mirror=${DEBIAN_MIRROR}
    - |
        if [ -f .git/refs/heads/pristine-lfs ] ; then
          # Avoid pack-objects running out of memory on bigger repositories
          git config pack.windowMemory 512m
          # Work around git-lfs in buster erroring out if lfs files are missing
          # even if they're on the server.
          git config lfs.allowincompletepush true
          git push --follow-tags ${CI_AUTH_PROJECT_URL} pristine-lfs
          git push --follow-tags ${CI_AUTH_PROJECT_URL} upstream/${UPSTREAM_RELEASE##*/}
        else
          echo "No pristine-lfs; skipping"
        fi
    - git push --follow-tags ${CI_AUTH_PROJECT_URL}
        $(git branch -l --format='%(refname:lstrip=2)'
          ${UPSTREAM_RELEASE}
          ${UPSTREAM_RELEASE}-security
          ${UPSTREAM_RELEASE}-proposed-updates)

merge-updates:
  stage: merge
  resource_group: $CI_PROJECT_ID
  tags:
    - lightweight
  variables:
    # needed to ensure the current branch has been refreshed, as the previous job may have update it
    # by importing updates and we do not want to try to merge an outdated commit
    # (see also how we pass the ci.skip options to not re-kick this pipeline when importing updates)
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - if: *mirror_branch_rule
      when: on_success
  script:
    - UPSTREAM_RELEASE=$CI_COMMIT_BRANCH
    - DOWNSTREAM_RELEASES_VAR=DOWNSTREAM_RELEASES_$(echo $UPSTREAM_RELEASE|sed "s@/@_@") # look for variables like "$DOWNSTREAM_RELEASES_debian_bullseye"
    - AUTO_MERGE_ARG=""
    - |
      if [ -z "$DOWNSTREAM_RELEASES" ]
      then
        echo "Checking downstream targets in ${DOWNSTREAM_RELEASES_VAR}"
        DOWNSTREAM_RELEASES=${!DOWNSTREAM_RELEASES_VAR} # expand variables like DOWNSTREAM_RELEASES_debian_bullseye
      fi
    - |
      if [ -z "$DOWNSTREAM_RELEASES" ]
      then
        echo "Falling back to $CI_DEFAULT_BRANCH as the downstream target"
        DOWNSTREAM_RELEASES=$CI_DEFAULT_BRANCH
      fi
    - |
      if [ -z "${PKG_UPDATES_AUTOMERGE_DISABLED}" ] && [ -n "${PKG_UPDATES_AUTOMERGE}" ]
      then
        echo "Enabling auto merge of trivial changes for ${PKG_UPDATES_AUTOMERGE}"
        AUTO_MERGE_ARG="--auto-merge=${PKG_UPDATES_AUTOMERGE}"
      fi
    - >
      apertis-pkg-merge-upstream-to-downstreams
      --osname=${OSNAME}
      --upstream=${UPSTREAM_RELEASE}
      --downstreams=${DOWNSTREAM_RELEASES}
      --stable=${STABLE_RELEASES}
      --local-version-suffix=${PKG_VERSION_SUFFIX}
      ${AUTO_MERGE_ARG}
      ${CI_AUTH_PROJECT_URL}

.apt-source:
  before_script:
    # Add apt source repository as it's not set in the docker image.
    - APT_SOURCE_TMP=$(mktemp)
    - echo "deb https://repositories.apertis.org/rpi64-containers/ v2021 rpi64-containers" >> $APT_SOURCE_TMP
    - cat /etc/apt/sources.list >> $APT_SOURCE_TMP
    - mv $APT_SOURCE_TMP /etc/apt/sources.list
    # debhelper-compat b-d is not being pulled, do it manually.
    - apt update && apt install --no-install-recommends -y debhelper

build-source:
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  stage: build
  extends:
    - .apt-source
  tags:
    - lightweight
  script:
    - |
      if dpkg-parsechangelog -c1 | grep -q 'PLEASE SUMMARIZE'
      then
        echo "debian/changelog says PLEASE SUMMARIZE, fix it"
        exit 1
      fi
    - TAG=$(git describe --tags --match "$OSNAME/*" --exact || true)
    - if [ -n "$TAG" ]; then echo "Skip building the package, already tagged as '$TAG'"; exit 0; fi
    - ci-buildpackage --git-builder='debuild --no-lintian -i -I' -S --git-prebuild='debian/rules debian/control || true'
    - mv _build/ _build-snapshot/
  artifacts:
    paths:
      - _build-snapshot/
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: *mirror_branch_rule
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^upstream\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^upstream\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^debian\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^debian\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^pristine-/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^pristine-/
      when: never
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
    - when: never

scan-licenses:
  stage: license scan
  tags:
    - lightweight
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: $CI_COMMIT_BRANCH =~ /^proposed-updates// || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^proposed-updates//
      when: on_success
    - when: never
  dependencies:
    - prepare-build-env
  script:
    # the license blacklist can be set on per-project/per-section basic
    - dpkg-source --before-build .
    - |
      if [ -e debian/apertis/pre-scan-hook ]
      then
        echo "Executing debian/apertis/pre-scan-hook"
        debian/apertis/pre-scan-hook
      fi
    - FAIL_ON_UNKNOWN=--fail-on-unknown
    # do not fail on unknown licenses if we do not have a blacklist anyway
    - test -z "${LICENSE_BLACKLIST}" && FAIL_ON_UNKNOWN=""
    - BRANCH=${CI_COMMIT_BRANCH:-$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    - case $APERTIS_RELEASE in
        v2020*)
          ci-license-scan
            --blacklist-licenses "${LICENSE_BLACKLIST}"
        ;;
        *)
          ci-license-scan --fossology-host "${FOSSOLOGY_URL}"
            --fossology-username "${FOSSOLOGY_USERNAME}" --fossology-password "${FOSSOLOGY_PASSWORD}"
            --source-url "https://${CI_SERVER_HOST}/${CI_PROJECT_PATH}" --source-branch "${BRANCH}"
            $FAIL_ON_UNKNOWN
            --blacklist-licenses "${LICENSE_BLACKLIST}"
        ;;
      esac
    - dpkg-source --after-build .
    - mv debian/apertis/copyright.new debian/apertis/copyright
    - git add -f debian/apertis/copyright
    - |
      if git commit -s -m 'Refresh the automatically detected licensing information' debian/apertis/copyright
      then
         set -x
         git push ${CI_AUTH_PROJECT_URL} HEAD:${BRANCH}
         set +x
      fi
  artifacts:
    when: on_failure
    paths:
      - debian/apertis/copyright
      - debian/apertis/copyright.new
      - debian/apertis/copyright.fossology
      - debian/fill.copyright.blanks.yml
      - debian/copyright-scan-patterns.yml

tag-release:
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
    RELEASE: 1
    PRISTINE_SOURCE: pristine-lfs-source
  # avoid conflicts when concurrently trying to commit stuff to the shared $PRISTINE_SOURCE branch
  resource_group: $CI_PROJECT_ID
  interruptible: false
  stage: release
  extends:
    - .apt-source
  tags:
    - lightweight
  script:
    - |
      if dpkg-parsechangelog -c1 -SDistribution | grep UNRELEASED
      then
        echo "debian/changelog says UNRELEASED, skip"
        exit 0
      fi
    - |
      if git describe --tags --match "$OSNAME/*" --exact-match > /dev/null 2>&1
      then
        echo "Fetching the already tagged source package version for this commit"
        pristine-lfs -v checkout --full --branch ${PRISTINE_SOURCE} --auto -o _build-release/
        exit 0
      fi
    - VERSIONTAG=$OSNAME/$(dpkg-parsechangelog -SVersion | sed -e 'y/:~/%_/' -e 's/\.\(\.\|$\|lock$\)/.\1#/g')
    - echo "debian/changelog points to ${VERSIONTAG}, looking for it"
    - |
      if git describe --tags --match "$OSNAME/*" --exact-match ${VERSIONTAG} > /dev/null 2>&1
      then
        echo "debian/changelog points to ${VERSIONTAG} which already exists, skip"
        exit 0
      fi
    - echo "Building the $(dpkg-parsechangelog -SVersion) source package"
    - gbp buildpackage
        --git-tag-only
        --git-ignore-branch
        --git-ignore-new
        --git-debian-tag="$OSNAME/%(version)s"
        --git-debian-tag-msg="%(pkg)s $OSNAME release %(version)s"
    - ci-buildpackage --git-builder='debuild --no-lintian -i -I' -S --git-prebuild='debian/rules debian/control || true'
    - mv _build/ _build-release/
    # Work around git-lfs in buster erroring out if lfs files are missing
    # even if they're on the server.
    - git config lfs.allowincompletepush true
    - pristine-lfs -v import-dsc --full --branch "${PRISTINE_SOURCE}" _build-release/*.dsc
    - git push --follow-tags "${CI_AUTH_PROJECT_URL}" "${PRISTINE_SOURCE}:${PRISTINE_SOURCE}"
    - git push "${CI_AUTH_PROJECT_URL}" "${VERSIONTAG}"
  artifacts:
    paths:
      - _build-release/
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: *osname_branch_rule
      changes:
        - debian/changelog
      when: on_success
    - when: never
  dependencies:
    - prepare-build-env

upload:
  stage: upload
  interruptible: false
  tags:
    - lightweight
  variables:
    GIT_DEPTH: 1
    JOBTEMPLATE: |
      obs-@ARCHITECTURE@-@REPOSITORY@:
        image: $DOCKER_IMAGE
        stage: build
        tags:
          - monitoring
        timeout: 1d # the LLVM build can take more than 20h
        retry:
          max: 2 # the OBS API can be quite flaky, try again when failing to see if the stars are better aligned
        variables:
          GIT_STRATEGY: none
        script:
          - osc-setup
          - echo '@APIURL@/package/show/@PROJECT@/@PACKAGE@' '@REPOSITORY@' '@ARCHITECTURE@' '@REVISION@' '@SRCMD5@'
          - osc-results () {
              osc results --verbose '@PROJECT@' '@PACKAGE@'
                --repo='@REPOSITORY@' --arch='@ARCHITECTURE@' --csv
                --format='%(code)s • %(dirty)s • %(repository)s • %(arch)s • %(state)s • %(details)s' || true;
            }
          - echo "📒 Current revision log for @APIURL@/package/show/@PROJECT@/@PACKAGE@"
          - osc log '@PROJECT@' '@PACKAGE@' --csv | head -n3 || true
          - touch build.log
          - |
            while true
            do
              echo "🔍 Check the status of @APIURL@/package/show/@PROJECT@/@PACKAGE@"
              osc-results | tee result.txt
              if grep -E '^\S+ • True' result.txt
              then
                echo "⏲️ OBS reported that the status need to be recomputed, wait a bit"
                sleep 30
                continue
              fi
              if osc api '/build/@PROJECT@/@REPOSITORY@/@ARCHITECTURE@/@PACKAGE@/_log?end=750' | grep "@SRCMD5@"
              then
                echo "📜 Retrieve the build log at offset `stat --format '%s' build.log`"
                echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
                osc remotebuildlog '@PROJECT@' '@PACKAGE@' '@REPOSITORY@' '@ARCHITECTURE@' --offset `stat --format '%s' build.log` | tee -a build.log || true
              else
                echo "📜 Build log for @APIURL@/package/show/@PROJECT@/@PACKAGE@ @REPOSITORY@/@ARCHITECTURE@ @SRCMD5@ not available"
              fi
              echo "📒 Check the current revision log for @APIURL@/package/show/@PROJECT@/@PACKAGE@"
              osc log '@PROJECT@' '@PACKAGE@' --csv | tee log.txt || true
              echo "🔍 Check the updated package status for @APIURL@/package/show/@PROJECT@/@PACKAGE@"
              osc-results | tee result.txt
              if ! head -n1 log.txt | grep '^@REVISION@|'
              then
                echo "🏳️ Revision @REVISION@ of @APIURL@/package/show/@PROJECT@/@PACKAGE@ is no longer current"
                break
              fi
              if grep -E '^(succeeded|failed|unresolvable|broken|disabled|excluded) • False' result.txt
              then
                echo "🏁 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ @REPOSITORY@/@ARCHITECTURE@ reached a final status"
                break
              fi
              echo "⏲️ Wait a bit before checking the status again"
              sleep 30
            done
          - osc api '/build/@PROJECT@/@REPOSITORY@/@ARCHITECTURE@/_jobhistory?package=@PACKAGE@' | tee buildhistory.txt
          - |
            if ! grep -q '<jobhist\b' buildhistory.txt
            then
              echo "No builds registered, the package may have never been rebuilt since branching"
              if grep ^succeeded result.txt
              then
                echo "🌟 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ was imported successfully on @REPOSITORY@/@ARCHITECTURE@"
                echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
                exit 0
              elif grep -E '^(disabled|excluded)' result.txt
              then
                echo "🌟 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ won't be built on @REPOSITORY@/@ARCHITECTURE@"
                echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
                exit 0
              else
                echo "🛑 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ was never built successfully on @REPOSITORY@/@ARCHITECTURE@"
                echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
                exit 1
              fi
            fi
          - grep 'rev="@REVISION@"' buildhistory.txt | tail -n1 | tee result.txt
          - |
            if ! grep 'code="succeeded"' result.txt
            then
              echo "🛑 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ didn't succeed on @REPOSITORY@/@ARCHITECTURE@"
              echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
              exit 1
            fi
          - echo "🌟 Package @APIURL@/package/show/@PROJECT@/@PACKAGE@ built successfully on @REPOSITORY@/@ARCHITECTURE@"
          - echo "  @APIURL@/package/live_build_log/@PROJECT@/@PACKAGE@/@REPOSITORY@/@ARCHITECTURE@"
        artifacts:
          paths:
            - build.log
          when: always
  script:
    - git describe --tags --match "$OSNAME/*" --exact && export RELEASE=1
    - |
      if [ "$RELEASE" == 1 ]
      then
        echo 'Release upload'
        DIR=release
      else
        echo 'Snapshot upload'
        DIR=snapshot
      fi
    - mv _build-$DIR/ _build/
    - PACKAGE=$(dpkg-parsechangelog -SSource)
    - osc-setup
    - echo Compute the upload destination
    - APIURL=$(osc config general apiurl | sed "s/.* is set to '\\(.*\\)'/\\1/")
    - SECTION=$(echo $CI_COMMIT_REF_NAME | sed -e 's|/|:|' -e 's|-|:|') # get 'apertis:v2020:security' from apertis/v2020-security
    - PROJECT="apertis:demo:rpi64-containers"
    - SNAPSHOT_PROJECT="$PROJECT:snapshots"
    - if [ "$RELEASE" != 1 ]; then PROJECT=$SNAPSHOT_PROJECT; fi
    - echo "Upload '$APIURL/package/show/$PROJECT/$PACKAGE' and clean up snapshot projects on release"
    - osc dput "$PROJECT" _build/*.dsc | tee upload.log
    - REVISION=$(awk '/Committed revision/ {sub(/\./,""); print $3}' upload.log)
    - |
      if [ -n "$REVISION" ]
      then
        echo "Package revision $REVISION of '$APIURL/package/show/$PROJECT/$PACKAGE' uploaded"
      else
        REVISION=$(osc log "$PROJECT" "$PACKAGE" --csv | sed 's/|.*//' | head -n1)
        echo "Package '$APIURL/package/show/$PROJECT/$PACKAGE' not uploaded, current revision is $REVISION"
      fi
    - SRCMD5=$(osc log "$PROJECT" "$PACKAGE" --csv -r "$REVISION" | cut -d '|' -f 4)
    - |
      if [ "$RELEASE" == 1 ]
      then
        echo "Clean up '$APIURL/package/show/$SNAPSHOT_PROJECT/$PACKAGE'"
        osc rdelete "$SNAPSHOT_PROJECT/$PACKAGE" -m "Landed $PACKAGE to $PROJECT" --force || true
      fi
    - echo Instantiate the build monitoring jobs
    - |
      osc repositories "$PROJECT" "$PACKAGE" |
        grep "^$OBS_REPOSITORIES" |
        while read REPOSITORY ARCHITECTURE
        do
          echo "$JOBTEMPLATE" | sed \
             -e "s|@APIURL@|$APIURL|" \
             -e "s/@PROJECT@/$PROJECT/" \
             -e "s/@PACKAGE@/$PACKAGE/" \
             -e "s/@REPOSITORY@/$REPOSITORY/" \
             -e "s/@ARCHITECTURE@/$ARCHITECTURE/" \
             -e "s/@REVISION@/$REVISION/" \
             -e "s/@SRCMD5@/$SRCMD5/"
        done | tee obs.yml
  artifacts:
    paths:
      - obs.yml
  environment:
    name: upload/$CI_COMMIT_REF_NAME
    # Ideally this would point to the obs project,
    # but we can't mangle variables enough
    url: https://build.collabora.co.uk
  rules: &upload_rules
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: *osname_branch_rule
      when: on_success
    - when: never
  dependencies:
    - prepare-build-env
    - tag-release
    - build-source

obs:
  stage: OBS
  needs:
    - upload
  trigger:
    strategy: depend
    include:
      - artifact: obs.yml
        job: upload
  rules: *upload_rules
